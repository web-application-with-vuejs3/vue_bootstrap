module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/~cs62160249/learn_bootstrap/'
    : '/'
}
